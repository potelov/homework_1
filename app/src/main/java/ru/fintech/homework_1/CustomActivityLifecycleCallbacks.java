package ru.fintech.homework_1;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

public class CustomActivityLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        Log.i(activity.getClass().getSimpleName(), "onCreate");
    }

    @Override
    public void onActivityStarted(Activity activity) {
        Log.i(activity.getClass().getSimpleName(), "onStart");
    }

    @Override
    public void onActivityResumed(Activity activity) {
        Log.i(activity.getClass().getSimpleName(), "onResume");
    }

    @Override
    public void onActivityPaused(Activity activity) {
        Log.i(activity.getClass().getSimpleName(), "onPause");
    }

    @Override
    public void onActivityStopped(Activity activity) {
        Log.i(activity.getClass().getSimpleName(), "onStop");
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        Log.i(activity.getClass().getSimpleName(), "onSaveInstanceState");
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Log.i(activity.getClass().getSimpleName(), "onDestroy");
    }
}
