package ru.fintech.homework_1;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private CustomActivityLifecycleCallbacks mCustomActivityLifecycleCallbacks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCustomActivityLifecycleCallbacks = new CustomActivityLifecycleCallbacks();
        getApplication().registerActivityLifecycleCallbacks(mCustomActivityLifecycleCallbacks);
        setUp();
    }

    private void setUp() {
        findViewById(R.id.btn_run).setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getApplication().unregisterActivityLifecycleCallbacks(mCustomActivityLifecycleCallbacks);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.activity_main);
        setUp();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_run) {
            openSecondActivity();
        }
    }

    private void openSecondActivity() {
        Intent intent = SecondActivity.getStartIntent(this);
        startActivity(intent);
    }
}
