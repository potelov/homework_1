package ru.fintech.homework_1;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {

    private Drawable mDrawable;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, SecondActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        setUp();
    }

    private void setUp() {
        mDrawable = (Drawable) getLastCustomNonConfigurationInstance();
        if (mDrawable == null) {
            mDrawable = getResources().getDrawable(R.drawable.image_legs_go_all, null);
        }
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return mDrawable;
    }
}
